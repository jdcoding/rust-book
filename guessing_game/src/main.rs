use rand::Rng;
use std::cmp::Ordering;
use std::io;
use std::str::FromStr;

fn main() {
    println!("I chose a number between 1 and 100. Can you guess it?");

    let secret_number = get_secret_number();

    loop {
        let guess = get_value_from_user();
        let guess = match guess {
            Ok(num) => num,
            Err(_) => {
                println!("I could not convert this into an unsigned integer. Please try again!");
                continue;
            }
        };
        println!("You guessed {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You got it. Congratulations!");
                break;
            }
        }
    }
}

fn get_secret_number() -> u32 {
    let n = rand::thread_rng().gen_range(1..101);
    return n;
}

fn get_value_from_user() -> Result<u32, <u32 as FromStr>::Err> {
    println!("Please input your guess!");
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");
    let guess: Result<u32, <u32 as FromStr>::Err> = guess
        // trim() removes the newline character(s) that is added when presing Enter.
        .trim()
        .parse();
    return guess;
}
